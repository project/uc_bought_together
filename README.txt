
---------------------------------------------------------------------------
ABOUT
---------------------------------------------------------------------------

This is a simple module to provide a block for use on UberCart product
pages that shows other products that were bought together with the
product you're viewing.


---------------------------------------------------------------------------
CONFIGURATION
---------------------------------------------------------------------------

Once the module is installed and enabled, visit the Block administration
page (admin/build/block):

Administer >> Site building >> Blocks
 
Find the block titled "UC: Products bought together" and place it in
whatever block region you want it to appear. It will produce no output
(and therefore not appear at all) unless you're viewing an UberCart
product node. If you click the "configure" link, can control how many
items appear in the block, if you want the block to include a count of
times the products were bought together, and if you want the block to
display titles, teasers, or full nodes.


---------------------------------------------------------------------------
CACHING
---------------------------------------------------------------------------

The block is configured to support block caching on a per-page basis.
Since it relies on a very expensive database query, it is strongly
recommended that you enable block caching on your site by visiting the
Performance administration page (admin/settings/performance):

Administer >> Site configuration >> Performance

Scroll down to the "Block cache" section and make sure it is enabled.


---------------------------------------------------------------------------
CREDITS
---------------------------------------------------------------------------

Written by Derek Wright http://drupal.org/user/46549


